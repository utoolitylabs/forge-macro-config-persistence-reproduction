import ForgeUI, {
  render,
  useConfig,
  MacroConfig,
  Fragment,
  Macro,
  Option,
  Select,
  Text,
  TextArea
} from '@forge/ui';

const Config = () => {
  return (
    <MacroConfig>
      <Select label="Select" name="selectValue" isRequired={true} >
        {["Select1", "Select2", "Select3"].map((selectValue) => (
          <Option label={selectValue} value={selectValue} />
        ))}
      </Select>
      <TextArea label="TextArea" name="textAreaValue" isMonospaced={true} isRequired={true} />
    </MacroConfig>
  );
};

const defaultConfig = {
  selectValue: "Select1",
  textAreaValue: "Some example textarea content"
}

const App = () => {
  console.log("App");
  const config = useConfig() || defaultConfig;
  console.log("... config: ", config);
  return <Fragment>
    <Text content={`Select was: ${config.selectValue}`} />
    <Text content={`TextArea was: ${config.textAreaValue}`} />
  </Fragment>
};

export const run = render(
  <Macro
    app={<App />}
  />
);

export const config = render(
  <Config />
);
