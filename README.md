# Forge macro config persistence problem reproduction (Typescript)

This project contains a Forge app written in Typescript that helps to reproduce a bug with Select component usage in a macro ConfigForm.

## Separate error for Forge UI >= 0.5.0

**NOTE:** The original reproduction (see below) is not valid anymore after switching to the new "editor macro experience" introduced with Forge UI >= 0.5.0.

However, switching to the new macro configuration approach as per [Switch to the new macro configuration](https://developer.atlassian.com/platform/forge/switch-to-the-new-macro-config/) revealed *that the new approach does not support `TextArea` components yet!* I.e. while it **probably** addresses the underlying issue, this reproduction still does not work, as it runs into a new/different error:

> Failed to load
>
> Expected direct child of MacroConfig to be one of [CheckboxGroup, DatePicker, RadioGroup, Select, TextField]. Received TextArea component. TextArea must be used within one of [Form, ConfigForm, CustomFieldEdit].

## Original reproduction steps for Forge UI < 0.5.0

Reproduction steps:

- Checkout repository
- `npm install`
- `forge register`
- `forge deploy`
- `forge install`
- Create a Confluence test page
- Add the macro, accepting the default values
- Publish the page - should display the "Select1" and respective textarea default values
- Edit the page, _changing only the macro's select value, e.g. to "Select3"_
- Publish again - should still show the the "Select1" and respective textarea default values as before!
- Repeat edit, changing the macro's select _and_ textarea value (but again w/o other page changes)
- Publish again - should still show the the "Select1" and respective textarea default values as before!
- Repeat any of the above, but _with_ an unrelated page change outside the macros
- Publish again - should now show changes as expected
- **NOTE:** I have encountered the _first_ select change going through on publish, but reproduction turned reliable after that, if nothing outside the macro was changed.

----
(Original Forge sample instructions below)

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

- Modify your app by editing the `src/index.tsx` file.

- Build and deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
